<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>InstallPage</name>
    <message>
        <location filename="../src/page/installpage.cpp" line="4"/>
        <source>正在安装</source>
        <translation>installing</translation>
    </message>
    <message>
        <location filename="../src/page/installpage.cpp" line="4"/>
        <source>本界面显示安装情况，安装成功后退出即可。</source>
        <translation>This interface shows the installation situation, You can exit after successful installation.</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/mainwindow.cpp" line="97"/>
        <source>打开文件夹</source>
        <translation>Open the folder</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="98"/>
        <source>程序文件夹</source>
        <translation>Program folder</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="99"/>
        <source>主目录文件夹</source>
        <translation>Home directory folder</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="100"/>
        <source>快捷方式文件夹</source>
        <translation>Shortcut folder</translation>
    </message>
</context>
<context>
    <name>OpenFilePage</name>
    <message>
        <location filename="../src/page/openfilepage.cpp" line="9"/>
        <source>请您打开软件包</source>
        <translation>Please open the package</translation>
    </message>
    <message>
        <location filename="../src/page/openfilepage.cpp" line="9"/>
        <source>拖动软件包到此或者使用文件选择对话框打开。</source>
        <translation>Drag a package here or open it using the File Selection dialog box.</translation>
    </message>
    <message>
        <location filename="../src/page/openfilepage.cpp" line="32"/>
        <source>下一步</source>
        <translation>Next</translation>
    </message>
    <message>
        <location filename="../src/page/openfilepage.cpp" line="33"/>
        <source>打开文件</source>
        <translation>Open the file</translation>
    </message>
    <message>
        <location filename="../src/page/openfilepage.cpp" line="68"/>
        <source>打开软件包</source>
        <translation>Open a package</translation>
    </message>
    <message>
        <location filename="../src/page/openfilepage.cpp" line="68"/>
        <source>软件包(*.appimage)</source>
        <translation>Application Package (*.appimage)</translation>
    </message>
    <message>
        <location filename="../src/page/openfilepage.cpp" line="106"/>
        <source>文件路径：</source>
        <translation>File path:</translation>
    </message>
</context>
<context>
    <name>OpenIconPage</name>
    <message>
        <location filename="../src/page/openiconpage.cpp" line="8"/>
        <source>文件</source>
        <translation>File</translation>
    </message>
    <message>
        <location filename="../src/page/openiconpage.cpp" line="10"/>
        <source>主题</source>
        <translation>Theme</translation>
    </message>
    <message>
        <location filename="../src/page/openiconpage.cpp" line="15"/>
        <source>请您选择程序图标</source>
        <translation>Please select the program icon</translation>
    </message>
    <message>
        <location filename="../src/page/openiconpage.cpp" line="15"/>
        <source>您可以指定图片或者通过主题选择图标。</source>
        <translation>You can specify a picture or select an icon by theme.</translation>
    </message>
    <message>
        <location filename="../src/page/openiconpage.cpp" line="18"/>
        <source>来源：</source>
        <translation>From:</translation>
    </message>
    <message>
        <location filename="../src/page/openiconpage.cpp" line="25"/>
        <source>下一步</source>
        <translation>Next</translation>
    </message>
    <message>
        <location filename="../src/page/openiconpage.cpp" line="26"/>
        <location filename="../src/page/openiconpage.cpp" line="69"/>
        <source>打开图标</source>
        <translation>Open icon</translation>
    </message>
    <message>
        <location filename="../src/page/openiconpage.cpp" line="69"/>
        <source>图标文件(*.png *.svg)</source>
        <translation>Icon file (*.png *.svg)</translation>
    </message>
    <message>
        <location filename="../src/page/openiconpage.cpp" line="130"/>
        <location filename="../src/page/openiconpage.cpp" line="191"/>
        <source>请将图标文件拖动到此</source>
        <translation>Please drag the icon file here</translation>
    </message>
    <message>
        <location filename="../src/page/openiconpage.cpp" line="157"/>
        <source>图标名称：</source>
        <translation>Icon name:</translation>
    </message>
    <message>
        <location filename="../src/page/openiconpage.cpp" line="214"/>
        <source>图标路径：</source>
        <translation>Icon path:</translation>
    </message>
</context>
<context>
    <name>RegFileTypePage</name>
    <message>
        <location filename="../src/page/regfiletypepage.cpp" line="3"/>
        <source>注册文件类型</source>
        <translation>Registration file type</translation>
    </message>
    <message>
        <location filename="../src/page/regfiletypepage.cpp" line="3"/>
        <source>注册该软件配套使用的文件类型，如办公套件应注册*.ppt、*.doc等文件类型。</source>
        <translation>Register the file type used by the software, such as office suite should register *. PPT, *. DOC and other file types.</translation>
    </message>
    <message>
        <location filename="../src/page/regfiletypepage.cpp" line="10"/>
        <source>安装</source>
        <translation>Install</translation>
    </message>
</context>
<context>
    <name>SetBaseInfoPage</name>
    <message>
        <location filename="../src/page/setbaseinfopage.cpp" line="5"/>
        <source>设置基本信息</source>
        <translation>Setting up basic information</translation>
    </message>
    <message>
        <location filename="../src/page/setbaseinfopage.cpp" line="5"/>
        <source>请输入程序的名称并选择其所属类别。</source>
        <translation>Please enter the name of the program and select the category to which it belongs.</translation>
    </message>
    <message>
        <location filename="../src/page/setbaseinfopage.cpp" line="11"/>
        <source>软件名称：</source>
        <translation>Application name:</translation>
    </message>
    <message>
        <location filename="../src/page/setbaseinfopage.cpp" line="12"/>
        <source>软件类别：</source>
        <translation>Software category:</translation>
    </message>
    <message>
        <location filename="../src/page/setbaseinfopage.cpp" line="13"/>
        <source>下一步</source>
        <translation>Next</translation>
    </message>
    <message>
        <location filename="../src/page/setbaseinfopage.cpp" line="42"/>
        <source>网络应用</source>
        <translation>Network</translation>
    </message>
    <message>
        <location filename="../src/page/setbaseinfopage.cpp" line="43"/>
        <source>社交沟通</source>
        <translation>Social communication</translation>
    </message>
    <message>
        <location filename="../src/page/setbaseinfopage.cpp" line="44"/>
        <source>音乐欣赏</source>
        <translation>Music</translation>
    </message>
    <message>
        <location filename="../src/page/setbaseinfopage.cpp" line="45"/>
        <source>视频播放</source>
        <translation>Video</translation>
    </message>
    <message>
        <location filename="../src/page/setbaseinfopage.cpp" line="46"/>
        <source>图形图像</source>
        <translation>Graphics</translation>
    </message>
    <message>
        <location filename="../src/page/setbaseinfopage.cpp" line="47"/>
        <source>游戏娱乐</source>
        <translation>Game</translation>
    </message>
    <message>
        <location filename="../src/page/setbaseinfopage.cpp" line="48"/>
        <source>办公学习</source>
        <translation>Office or study</translation>
    </message>
    <message>
        <location filename="../src/page/setbaseinfopage.cpp" line="49"/>
        <source>阅读翻译</source>
        <translation>Reading and translation</translation>
    </message>
    <message>
        <location filename="../src/page/setbaseinfopage.cpp" line="50"/>
        <source>编程开发</source>
        <translation>Development</translation>
    </message>
    <message>
        <location filename="../src/page/setbaseinfopage.cpp" line="51"/>
        <source>系统管理</source>
        <translation>System</translation>
    </message>
    <message>
        <location filename="../src/page/setbaseinfopage.cpp" line="52"/>
        <location filename="../src/page/setbaseinfopage.cpp" line="53"/>
        <source>其他应用</source>
        <translation>Other</translation>
    </message>
</context>
<context>
    <name>StepList</name>
    <message>
        <location filename="../src/widget/steplist.cpp" line="9"/>
        <source>打开软件包</source>
        <translation>Open the package</translation>
    </message>
    <message>
        <location filename="../src/widget/steplist.cpp" line="10"/>
        <source>选择图标文件</source>
        <translation>Select icon file</translation>
    </message>
    <message>
        <location filename="../src/widget/steplist.cpp" line="11"/>
        <source>设置基本信息</source>
        <translation>Setting up basic information</translation>
    </message>
    <message>
        <location filename="../src/widget/steplist.cpp" line="12"/>
        <source>注册文件类型</source>
        <translation>Registration file type</translation>
    </message>
    <message>
        <location filename="../src/widget/steplist.cpp" line="13"/>
        <source>完成</source>
        <translation>Finish</translation>
    </message>
</context>
</TS>
