# appimage-installer

#### Description

A very convenient appimage software deployment tool.

#### Software Architecture

The software is developed using DTK.

### Compile and install

```bash
./build.sh
```

After compiling and installing, the folder build and a DEB file will be generated in the current directory,

This DEB package only supports the installation and normal use of deepin / UOS.

#### Function Description

The software itself provides the unpacking function of AppImage, in order to quickly and conveniently obtain the information such as icons in the package.

The specific directory of the software deployment is as follow：

```
软件主体：$XDG_DATA_HOME/appimages

软件图标：$XDG_DATA_HOME/appimages/icons

快捷方式：$XDG_DATA_HOME/applications
```

The specific directory of the software deployment is as follow.

For the sake of convenience，Software menu can be quickly open `$XDG_DATA_HOME/appimages（Software Folder）` and `$XDG_DATA_HOME/applications（Shortcut Folder）`Two folder，This is used to remove or modify deployed software。

#### Contribution

1.  Fork the repository