#include "mainwindow.h"
#include <QLayout>
#include <DGuiApplicationHelper>
#include <QDesktopServices>
#include <DWidgetUtil>
#include <QMenu>
MainWindow::MainWindow(QWidget *parent)
    : DBlurEffectWidget(parent)
{
    titlebar=new DTitlebar();
    stk=new QStackedWidget();
    QLabel* logo=new QLabel;
    QHBoxLayout* layout=new QHBoxLayout;
    QVBoxLayout* left=new QVBoxLayout;
    QVBoxLayout* right=new QVBoxLayout;
    
    logo->setPixmap(QIcon::fromTheme("appimage-installer").pixmap(64,64));
    logo->setMargin(20);
    logo->setAlignment(Qt::AlignCenter);
    // titlebar->setIcon(QIcon::fromTheme("appimage-installer"));

    setWindowIcon(QIcon::fromTheme("appimage-installer"));



    left->addWidget(logo);
    right->addWidget(titlebar);
    right->addWidget(stk);
    layout->addLayout(left);
    layout->addLayout(right);
    setLayout(layout);
    layout->setMargin(0);
    left->setMargin(0);
    right->setMargin(0);
    right->setSpacing(0);

    logo->setFixedSize(200,100);
    setFixedSize(800,590);
    moveToCenter(this);

    //stepList
    stepList=new StepList(this);
    left->addWidget(stepList);
    setMaskAlpha(200);


    openFilePage=new OpenFilePage(this);
    openIconPage=new OpenIconPage(this);
    setBaseInfoPage=new SetBaseInfoPage(this);
    regFileTypePage=new RegFileTypePage(this);
    installPage=new InstallPage(this);
    stk->addWidget(openFilePage);
    stk->addWidget(openIconPage);
    stk->addWidget(setBaseInfoPage);
    stk->addWidget(regFileTypePage);
    stk->addWidget(installPage);


    stk->setCurrentWidget(openFilePage);

    connect(openFilePage,&OpenFilePage::next,[=](){
        if(!AppImage::file->icon().path.isEmpty()){
            openIconPage->openIconFile(AppImage::file->icon().path);
        }
        stepList->setNowStep(StepList::Btn::OpenIcon);
        stk->setCurrentWidget(openIconPage);
    });
    connect(openIconPage,&OpenIconPage::next,[=](AppIcon icon){
        stepList->setNowStep(StepList::Btn::SetInfo);
        stk->setCurrentWidget(setBaseInfoPage);
        AppImage::file->setIcon(icon);
    });
    connect(setBaseInfoPage,&SetBaseInfoPage::next,[=](BaseInfo info){
        stepList->setNowStep(StepList::Btn::RegFile);
        stk->setCurrentWidget(regFileTypePage);
        regFileTypePage->list->clearItems();
        for(auto m : AppImage::file->regFiles){
            regFileTypePage->list->addItem(QIcon::fromTheme(m.iconName).pixmap(100,100),m.comment,m.exname);
        }
        AppImage::file->setName(info.name);
        AppImage::file->setCategories(info.categories);
        AppImage::file->setWMClass(info.wmclass);
        if(AppImage::file->regFiles.size()==0){
            emit regFileTypePage->next();
        }
    });
    connect(regFileTypePage,&RegFileTypePage::next,[=](){
        stepList->setNowStep(StepList::Btn::Finish);
        stk->setCurrentWidget(installPage);
        if(AppImage::file->install()){
            installPage->setTitle("安装完成","您现在可以关闭窗口并从启动器打开安装的软件。"); 
        }else{
            installPage->setTitle("安装失败","您可以在深度科技论坛反馈您遇到的问题，我需要下面这些信息来帮助你：\n1.您安装的软件\n2.您的系统版本\n3.您的安装包来源\n您可以在论坛@Maicss以尽快通知到我。"); 
        }
        installPage->btn_Next->setEnabled(true);
    });
    QMenu *menu=new QMenu;
    titlebar->setMenu(menu);
    QMenu *menu_opendir=new QMenu(tr("打开文件夹"));
    QAction *action_appdir=new QAction(tr("程序文件夹"));
//    QAction *action_homesdir=new QAction(tr("主目录文件夹"));
    QAction *action_desktopdir=new QAction(tr("快捷方式文件夹"));
    menu->addMenu(menu_opendir);
    menu_opendir->addActions({action_appdir,action_desktopdir});

    QDir dataDir(Core::env("XDG_DATA_HOME")[0]);
    QDir target_app(dataDir.path() + "/appimages");         //用于存放App可执行文件
    QDir target_desktop(dataDir.path() + "/applications");  //用于输出desktop文件的目录
    QDir target_homes(dataDir.path() + "/appimages/homes"); //用于存放各个虚拟目录
    connect(action_appdir,&QAction::triggered,[=](){QDesktopServices::openUrl(QUrl(target_app.path()));});
    connect(action_desktopdir,&QAction::triggered,[=](){QDesktopServices::openUrl(QUrl(target_desktop.path()));});
//    connect(action_homesdir,&QAction::triggered,[=](){QDesktopServices::openUrl(QUrl(target_homes.path()));});
    updateStyle();

}

MainWindow::~MainWindow()
{
    Core::close();
}

void MainWindow::openFile(QString path)
{
    if(QFile::exists(path)){
        openFilePage->open(path);
    }
}
/*!
 * \brief MainWindow::updateStyle
 * 用于设置刷新样式
 */
void MainWindow::updateStyle()
{
    connect(DGuiApplicationHelper::instance(),&DGuiApplicationHelper::themeTypeChanged,[=](DGuiApplicationHelper::ColorType type){
        stk->setStyleSheet(""); //没这条会导致page内插件样式错误
        if(type==DGuiApplicationHelper::ColorType::DarkType){
            stk->setStyleSheet(".QStackedWidget{background-color:#282828}");
        }else {
            stk->setStyleSheet(".QStackedWidget{background-color:#FFFFFF}");
        }
    });
    //主要是解决DTK的bug，重新设置样式防止样式初始化错误
    emit DGuiApplicationHelper::instance()->themeTypeChanged(DGuiApplicationHelper::instance()->themeType());
}
