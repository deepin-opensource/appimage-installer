#pragma once
#include <QWidget>
#include <QObject>
#include <QLineEdit>
#include <QComboBox>
#include <QPushButton>
#include <DSuggestButton>
#include <QCheckBox>
#include "page.h"
struct BaseInfo {
    QString name;
    QString categories;
    QString wmclass;
};
class SetBaseInfoPage : public Page
{
    Q_OBJECT
public:
    SetBaseInfoPage(QWidget* parent=nullptr);
private:
    QLineEdit* line_Name;
    QComboBox* cbb_Categories;
    QLineEdit* line_WMClass;
    QPushButton* btn_getWMClass;
    QCheckBox* ckb_update;
    QComboBox* cbb_installed;

    DSuggestButton *btn_Next;
signals:
    void next(BaseInfo info);
};
