#include "installpage.h"
#include <DApplication>
InstallPage::InstallPage(QWidget* parent) : Page(parent){
    setTitle(tr("正在安装"),tr("本界面显示安装情况，安装成功后退出即可。"));
    QWidget* w=new QWidget;
    QVBoxLayout* layout=new QVBoxLayout;
    w->setLayout(layout);
    setPageWidget(w);
    btn_Next=new DSuggestButton("关闭");

    btn_Next->setEnabled(false);

    btn_Next->setFixedSize(145,40);
    QHBoxLayout* nextBtn_layout=new QHBoxLayout;
    nextBtn_layout->addStretch();
    nextBtn_layout->addWidget(btn_Next);
    nextBtn_layout->setSpacing(20);
    layout->addSpacing(50);
    layout->addStretch();
    layout->addLayout(nextBtn_layout);
    connect(btn_Next,&DSuggestButton::clicked,[](){
        DApplication::exit();
        Core::close();
    });
}
