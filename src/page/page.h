#pragma once
#include <QWidget>
#include <QLayout>
#include "src/core/core.h"
//该类是所有page的基类，用于创建基本骨架，保持风格一致的同时减少重复代码
class Page : public QWidget
{
    Q_OBJECT
public:
    Page(QWidget *parent = nullptr);
    void setTitle(QString t,QString ti);
    void setPageWidget(QWidget* p); //设置内容部分的Widget
private:
    QVBoxLayout* layout;
    QLabel* title;
    QLabel* tip;

signals:
public slots:
};
