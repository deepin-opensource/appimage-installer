#pragma once
#include <QWidget>
#include <QObject>
#include <QPushButton>
#include <DSuggestButton>
#include "page.h"
#include "src/widget/regfileview.h"
class RegFileTypePage : public Page
{
    Q_OBJECT
public:
    RegFileTypePage(QWidget* parent=nullptr);
    RegFileView* list;
private:
    DSuggestButton *btn_Next;
signals:
    void next();
};
