#include "page.h"

Page::Page(QWidget* parent) : QWidget(parent){
    title=new QLabel;
    title->setFont(Core::font(Core::FontType::TitleFont));
    title->setText(tr(""));
    title->setFixedHeight(46);
    tip=new QLabel;
    tip->setFont(Core::font(Core::FontType::TipFont));
    tip->setText(tr(""));
    tip->setFixedHeight(28);
    layout=new QVBoxLayout(this);
    layout->addWidget(title);
    layout->addSpacing(10);
    layout->addWidget(tip);
    layout->addSpacing(20);
    layout->setContentsMargins(20,8,24,24);
}
void Page::setTitle(QString t,QString ti){
    title->setText(t);
    tip->setText(ti);
}
void Page::setPageWidget(QWidget* p){
    layout->addWidget(p);
}