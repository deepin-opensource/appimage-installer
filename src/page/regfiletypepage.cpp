#include "regfiletypepage.h"
RegFileTypePage::RegFileTypePage(QWidget* parent) : Page(parent){
    setTitle(tr("注册文件类型"),tr("注册该软件配套使用的文件类型，如办公套件应注册*.ppt、*.doc等文件类型。"));
    QWidget* w=new QWidget;
    QVBoxLayout* layout=new QVBoxLayout;
    w->setLayout(layout);
    setPageWidget(w);
    list=new RegFileView();
    layout->addWidget(list);
    btn_Next=new DSuggestButton(tr("安装"));

    btn_Next->setFixedSize(145,40);
    QHBoxLayout* nextBtn_layout=new QHBoxLayout;
    nextBtn_layout->addStretch();
    nextBtn_layout->addWidget(btn_Next);
    nextBtn_layout->setSpacing(20);
    layout->addSpacing(50);
    layout->addLayout(nextBtn_layout);
    connect(btn_Next,&QPushButton::clicked,this,&RegFileTypePage::next);
}
