#pragma once
#include <QWidget>
#include <QObject>
#include <QPushButton>
#include <DSuggestButton>
#include "page.h"
#include "src/widget/regfileview.h"
class InstallPage : public Page
{
    Q_OBJECT
public:
    InstallPage(QWidget* parent=nullptr);
    DSuggestButton *btn_Next;
private:
signals:
    void next();
};
