#include "openiconpage.h"
#include <QLayout>
#include "src/core/appimage.h"
OpenIconPage::OpenIconPage(QWidget *parent) : Page(parent)
{
    QLabel *title = new QLabel;
    btn_fromFile = new QRadioButton(this);
    btn_fromFile->setText(tr("文件"));
    btn_fromTheme = new QRadioButton(this);
    btn_fromTheme->setText(tr("主题"));
    stkFrom = new QStackedWidget;

    w_fromFile = new QWidget(this);
    w_fromTheme = new QWidget(this);
    setTitle(tr("请您选择程序图标"), tr("您可以指定图片或者通过主题选择图标。"));

    QHBoxLayout *btn_layout = new QHBoxLayout;
    QLabel *from = new QLabel(tr("来源："));
    btn_layout->addWidget(from);
    btn_layout->addWidget(btn_fromFile);
    btn_layout->addWidget(btn_fromTheme);
    btn_fromFile->setChecked(true);
    btn_layout->addStretch();

    btn_next = new DSuggestButton(tr("下一步"));
    btn_openIcon = new QPushButton(tr("打开图标"));
    QHBoxLayout *nextBtn_layout = new QHBoxLayout;
    btn_next->setFixedSize(145, 40);
    btn_openIcon->setFixedSize(145, 40);
    nextBtn_layout->addStretch();
    nextBtn_layout->addWidget(btn_openIcon);
    nextBtn_layout->addWidget(btn_next);
    nextBtn_layout->setSpacing(20);
    QWidget *w = new QWidget;
    layout = new QVBoxLayout;
    layout->addLayout(btn_layout);
    layout->addWidget(stkFrom);
    //    layout->addStretch();
    layout->addLayout(nextBtn_layout);
    stkFrom->addWidget(w_fromFile);
    stkFrom->addWidget(w_fromTheme);

    // layout->setContentsMargins(20,8,24,24);
    w->setLayout(layout);
    setPageWidget(w);
    connect(btn_fromFile, &QRadioButton::clicked, [=]() {
        stkFrom->setCurrentWidget(w_fromFile);
        if (dropIcon_Path->text().isEmpty())
        {
            btn_next->setEnabled(false);
        }
        else
        {
            btn_next->setEnabled(true);
        }
    });
    connect(btn_fromTheme, &QRadioButton::clicked, [=]() {
        stkFrom->setCurrentWidget(w_fromTheme);
        if (*showIcon->pixmap() == QIcon::fromTheme("application-x-executable").pixmap(128, 128))
        {
            btn_next->setEnabled(false);
        }
        else
        {
            btn_next->setEnabled(true);
        }
    });
    connect(btn_openIcon, &QPushButton::clicked, [=]() {
        QString p = Core::openFile(tr("打开图标"), tr("图标文件(*.png *.svg)"));
        if (!p.isNull())
        {
            OpenIconPage::openIconFile(p);
        }
    });
    connect(btn_next, &QPushButton::clicked, [=]() {
        AppIcon icon;
        if (btn_fromTheme->isChecked())
        {
            //来自主题
            icon.type = FromTheme;
            icon.path = themeEdit->text();
        }
        else
        {
            //来自文件
            icon.type = FromFile;
            icon.path = OpenIconPage::iconFile_str;
        }
        icon.pixmap = QPixmap();
        emit next(icon);
    });
    shadow_effect = new QGraphicsDropShadowEffect(this);
    setAcceptDrops(true);
    initFromFilePage();
    initFromThemePage();
    btn_next->setEnabled(false);
}

void OpenIconPage::dragEnterEvent(QDragEnterEvent *event)
{
    if (Core::getFileType(QUrl(event->mimeData()->text()).path()) == Core::FileType::TypePng)
    {
        dropIcon->setPixmap(QUrl(event->mimeData()->text()).path());
        dropIcon->setPixmap((*dropIcon->pixmap()).scaled(128, 128));
        setHover(dropIcon);
        event->acceptProposedAction();
    }
    else if (Core::getFileType(QUrl(event->mimeData()->text()).path()) == Core::FileType::TypeSvg)
    {
        dropIcon->setPixmap(Core::loadSvg(QUrl(event->mimeData()->text()).path()));
        setHover(dropIcon);
        event->acceptProposedAction();
    }
    else
    {
        setHover(nullptr);
    }
}

void OpenIconPage::dropEvent(QDropEvent *event)
{
    setHover(nullptr);
    openIconFile(QUrl(event->mimeData()->text()).path());
}

void OpenIconPage::dragLeaveEvent(QDragLeaveEvent *event)
{
    Q_UNUSED(event)

    dropIcon->setText(tr("请将图标文件拖动到此"));
    dropIcon->setStyleSheet("QLabel{border:2px dashed rgba(130,130,130,.4);}");
    setHover(nullptr);
    if (Core::getFileType(iconFile_str) == Core::FileType::TypePng)
    {
        dropIcon->setPixmap(iconFile_str);
        dropIcon->setPixmap((*dropIcon->pixmap()).scaled(128, 128));
    }
    else if (Core::getFileType(iconFile_str) == Core::FileType::TypeSvg)
    {
        dropIcon->setPixmap(Core::loadSvg(iconFile_str));
    }
}

void OpenIconPage::initFromThemePage()
{
    showIcon = new QLabel;
    showIcon->setPixmap(QIcon::fromTheme("application-x-executable").pixmap(128, 128));
    QVBoxLayout *layout_fromTheme = new QVBoxLayout;

    layout_fromTheme->addSpacing(30);
    layout_fromTheme->addWidget(showIcon);
    layout_fromTheme->addSpacing(30);
    showIcon->setMinimumSize(128, 128);
    showIcon->setAlignment(Qt::AlignCenter);
    QHBoxLayout *inputLayout = new QHBoxLayout;
    QLabel *inputLabel = new QLabel;
    inputLabel->setText(tr("图标名称："));
    themeEdit = new QLineEdit();
    inputLayout->addStretch();
    inputLayout->addWidget(inputLabel);
    inputLayout->addWidget(themeEdit);
    inputLayout->addStretch();
    themeEdit->setFixedWidth(230);
    connect(themeEdit, &QLineEdit::textChanged, [=](QString text) {
        QPixmap p = QIcon::fromTheme(text).pixmap(128, 128);
        if (!p.isNull())
        {
            themeIcon_str = "";
            showIcon->setPixmap(p);
            btn_next->setEnabled(true);
        }
        else
        {
            themeIcon_str = text;
            btn_next->setEnabled(false);
            showIcon->setPixmap(QIcon::fromTheme("application-x-executable").pixmap(128, 128));
        }
    });

    layout_fromTheme->addLayout(inputLayout);
    layout_fromTheme->addStretch();
    layout_fromTheme->setAlignment(Qt::AlignHCenter);
    w_fromTheme->setLayout(layout_fromTheme);
}

void OpenIconPage::initFromFilePage()
{
    dropIcon = new QLabel;
    dropIcon_Path = new QLabel;
    dropIcon->setStyleSheet("QLabel{border:2px dashed rgba(130,130,130,.4);}");
    dropIcon->setText(tr("请将图标文件拖动到此"));
    dropIcon->setAlignment(Qt::AlignCenter);
    dropIcon_Path->setFont(Core::font(Core::FontType::TipFont));
    dropIcon_Path->setWordWrap(true);
    dropIcon_Path->setFixedHeight(100);
    QVBoxLayout *layout_fromFile = new QVBoxLayout;
    layout_fromFile->addWidget(dropIcon);
    layout_fromFile->addWidget(dropIcon_Path);
    w_fromFile->setLayout(layout_fromFile);
}

void OpenIconPage::openIconFile(QString path)
{
    if (Core::getFileType(path) == Core::FileType::TypePng)
    {
        dropIcon->setPixmap(path);
        dropIcon->setPixmap((*dropIcon->pixmap()).scaled(128, 128));
    }
    else if (Core::getFileType(path) == Core::FileType::TypeSvg)
    {
        dropIcon->setPixmap(Core::loadSvg(path));
    }
    tmp_p = *dropIcon->pixmap();
    dropIcon_Path->setText(tr("图标路径：") + path);
    iconFile_str = path;
    btn_fromFile->setChecked(true);
    emit btn_fromFile->clicked(true);
}

void OpenIconPage::setHover(QLabel *l)
{
    QColor c;
    if (l == nullptr || l->pixmap()->isNull())
    {
        dropIcon->setStyleSheet("QLabel{border:2px dashed rgba(130,130,130,.4);}");
        c.setAlpha(0);
        shadow_effect->setColor(c);
        return;
    }
    dropIcon->setStyleSheet("QLabel{border:2px dashed " + Core::highlightColor().name() + ";}");
    shadow_effect->setOffset(0, 5);
    c = Core::pixmapMainColor(*l->pixmap(), float(1.8));
    c.setAlpha(255);
    shadow_effect->setColor(c);
    shadow_effect->setBlurRadius(100);
    l->setGraphicsEffect(shadow_effect);
}
