#ifndef OPENICONPAGE_H
#define OPENICONPAGE_H

#include <QWidget>
#include <QLayout>
#include <QAbstractAnimation>
#include <QRadioButton>
#include <QStackedWidget>
#include <QLineEdit>
#include <DSuggestButton>
#include <QPushButton>
#include "src/core/core.h"
#include "page.h"
#include "src/core/appimage.h"
class OpenIconPage : public Page
{
    Q_OBJECT
public:
    explicit OpenIconPage(QWidget *parent = nullptr);
    void dragEnterEvent(QDragEnterEvent *event);
    void dropEvent(QDropEvent *event);
    void dragLeaveEvent(QDragLeaveEvent *event);
    void openIconFile(QString path);
private:
    QVBoxLayout* layout;
    QRadioButton* btn_fromTheme;
    QRadioButton* btn_fromFile;
    QStackedWidget* stkFrom;
    QWidget* w_fromTheme;
    QWidget* w_fromFile;
    QLabel* dropIcon;
    QLabel* dropIcon_Path;
    QGraphicsDropShadowEffect *shadow_effect;
    DSuggestButton* btn_next;
    QPushButton* btn_openIcon;
    QLabel* showIcon;
    QPixmap tmp_p;
    QLineEdit *themeEdit;

    void initFromThemePage();
    void initFromFilePage();
    void setHover(QLabel *l);

    QString iconFile_str;
    QString themeIcon_str;
signals:
    void next(AppIcon icon);
public slots:
};

#endif // OPENICONPAGE_H
