#include "openfilepage.h"
#include <QLayout>
#include <QGraphicsDropShadowEffect>
#include <QIcon>
#include "src/core/appimage.h"
OpenFilePage::OpenFilePage(QWidget *parent) : Page(parent)
{
    spinner=new DSpinner;
    setTitle(tr("请您打开软件包"),tr("拖动软件包到此或者使用文件选择对话框打开。"));
    vndIcon=new QLabel;
    iso9660Icon=new QLabel;
    w_icon=new QWidget;
    QHBoxLayout *icons=new QHBoxLayout;
    icons->addWidget(vndIcon);
    icons->addWidget(iso9660Icon);
    vndIcon->setPixmap(QIcon::fromTheme("application-x-executable").pixmap(128,128));
    iso9660Icon->setPixmap(QIcon::fromTheme("application-x-iso9660-appimage").pixmap(128,128));
    icons->setSpacing(100);
    icons->setAlignment(Qt::AlignCenter);
    w_icon->setLayout(icons);
    w_icon->setStyleSheet("QWidget{border:2px dashed rgba(130,130,130,.4);}"
                          "QLabel{border:0;}");
    // w_icon->setFixedSize(505,230);
    w_icon->setFixedHeight(230);

    filePath=new QLabel;
//    filePath->setText(tr("文件路径："));
    filePath->setWordWrap(true);
    filePath->setFont(Core::font(Core::FontType::TipFont));
    filePath->setMaximumHeight(100);

    btn_Next=new DSuggestButton(tr("下一步"));
    btn_OpenFile=new QPushButton(tr("打开文件"));
    btn_Next->setFixedSize(145,40);
    btn_OpenFile->setFixedSize(145,40);
    btn_Next->setEnabled(false);

    QHBoxLayout *layout_btn=new QHBoxLayout;
    layout_btn->addStretch();
    layout_btn->addWidget(spinner);
    layout_btn->addWidget(btn_OpenFile);
    layout_btn->addWidget(btn_Next);
    layout_btn->setSpacing(20);
    spinner->setVisible(false);
    spinner->setFixedSize(40,40);
    QWidget *w=new QWidget;
    QVBoxLayout* layout=new QVBoxLayout;
    layout->addWidget(w_icon);
    layout->addSpacing(20);
    layout->addWidget(filePath);
    layout->addStretch();
    layout->addLayout(layout_btn);
    w->setLayout(layout);
    setPageWidget(w);
    // layout->setContentsMargins(20,8,24,24);

    setAcceptDrops(true);
//    setHover(vndIcon);
//    setHover(iso9660Icon);
    connect(btn_Next,&QPushButton::clicked,[=](){
        if(Core::isExist(path)){
            emit OpenFilePage::next(path);
        }else {
            btn_Next->setEnabled(false);
        }
    });
    connect(btn_OpenFile,&QPushButton::clicked,[=](){
        QString p=Core::openFile(tr("打开软件包"),tr("软件包(*.appimage)"));
        if(p.isEmpty()){
            return ;
        }
        open(p);

    });
    connect(AppImage::file,&AppImage::loadFinish,[=](){
        btn_Next->setEnabled(true);
        btn_OpenFile->setEnabled(true);
        setAcceptDrops(true);
        spinner->setVisible(false);
    });
}

void OpenFilePage::setHover(QLabel *l)
{
    vndIcon->setGraphicsEffect(nullptr);
    iso9660Icon->setGraphicsEffect(nullptr);
    if(l==nullptr){
        w_icon->setStyleSheet("QWidget{border:2px dashed rgba(130,130,130,.4);}"
                              "QLabel{border:0;}");
        return;
    }
    w_icon->setStyleSheet("QWidget{border:2px dashed "+Core::highlightColor().name()+";}"
                          "QLabel{border:0;}");
    QGraphicsDropShadowEffect *shadow_effect = new QGraphicsDropShadowEffect(this);
    shadow_effect->setOffset(0, 5);
    QColor c=Core::pixmapMainColor(*l->pixmap(),float(1.8));
    c.setAlpha(255);
    shadow_effect->setColor(c);
    shadow_effect->setBlurRadius(100);
    l->setGraphicsEffect(shadow_effect);
}

void OpenFilePage::open(QString path)
{
    this->path=path;
    filePath->setText(tr("文件路径：")+path);
    btn_Next->setEnabled(true);
    AppImage::file->load(path);
    btn_Next->setEnabled(false);
    btn_OpenFile->setEnabled(false);
    setAcceptDrops(false);
    spinner->start();
    spinner->setVisible(true);
}

void OpenFilePage::dragEnterEvent(QDragEnterEvent *event)
{
    if(Core::getFileType(QUrl(event->mimeData()->text()).path())==Core::FileType::TypeAppVnd){
        setHover(vndIcon);
        event->acceptProposedAction();
    }else if (Core::getFileType(QUrl(event->mimeData()->text()).path())==Core::FileType::TypeAppIso9660) {
        setHover(iso9660Icon);
        event->acceptProposedAction();
    }else {
        setHover(nullptr);
    }

}

void OpenFilePage::dropEvent(QDropEvent *event)
{
    dragLeaveEvent(nullptr);
    open(QUrl(event->mimeData()->text()).path());
}

void OpenFilePage::dragLeaveEvent(QDragLeaveEvent *event)
{
    Q_UNUSED(event)
    w_icon->setStyleSheet("QWidget{border:2px dashed rgba(130,130,130,.4);}"
                          "QLabel{border:0;}");
    setHover(nullptr);
}
