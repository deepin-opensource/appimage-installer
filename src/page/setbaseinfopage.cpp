#include "setbaseinfopage.h"
#include <QObject>
#include <DGroupBox>
SetBaseInfoPage::SetBaseInfoPage(QWidget *parent) : Page(parent)
{
    setTitle(tr("设置基本信息"), tr("请输入程序的名称并选择其所属类别。"));
    QWidget *w = new QWidget;
    QVBoxLayout *layout = new QVBoxLayout;
    w->setLayout(layout);
    setPageWidget(w);

    //页内空间的初始化
    QLabel *l_Name = new QLabel(tr("软件名称"));
    QLabel *l_Categories = new QLabel(tr("软件类别"));
    QLabel *l_WMClass = new QLabel(tr("窗口ID"));
    cbb_Categories = new QComboBox;
    line_Name = new QLineEdit;
    line_WMClass = new QLineEdit;
    btn_getWMClass = new QPushButton();
    ckb_update = new QCheckBox(tr("已安装该应用的旧版"));
    btn_Next=new DSuggestButton(tr("下一步"));
    QHBoxLayout *btn_layout=new QHBoxLayout;
    DGroupBox *box_name = new DGroupBox;
    DGroupBox *box_categoties = new DGroupBox;
    DGroupBox *box_wmclass = new DGroupBox;
    DGroupBox *box_update = new DGroupBox;
    cbb_installed = new QComboBox;

    //TEST
    cbb_installed->setEnabled(false);
    ckb_update->setEnabled(false);

    //配置样式
    btn_getWMClass->setToolTip("窗口ID是应用程序窗口在运行时的标识字符串，\n"
                               "需要您先手动运行程序，点击此按钮，后用十字\n"
                               "光标点击应用程序主窗口即可。");
    l_Name->setAlignment(Qt::AlignRight | Qt::AlignVCenter);
    l_Categories->setAlignment(Qt::AlignRight | Qt::AlignVCenter);
    l_WMClass->setAlignment(Qt::AlignRight | Qt::AlignVCenter);
    cbb_Categories->setMinimumWidth(180);
    cbb_Categories->addItem(tr("网络应用"), "Network");
    cbb_Categories->addItem(tr("社交沟通"), "Chat");
    cbb_Categories->addItem(tr("音乐欣赏"), "Audio");
    cbb_Categories->addItem(tr("视频播放"), "AudioVideo");
    cbb_Categories->addItem(tr("图形图像"), "Graphics");
    cbb_Categories->addItem(tr("游戏娱乐"), "Game");
    cbb_Categories->addItem(tr("办公学习"), "Office");
    cbb_Categories->addItem(tr("阅读翻译"), "Translation");
    cbb_Categories->addItem(tr("编程开发"), "Development");
    cbb_Categories->addItem(tr("系统管理"), "System");
    cbb_Categories->addItem(tr("其他应用"), "");
    cbb_Categories->setCurrentText(tr("其他应用")); //设置默认选项位于其他选项

    //布局
    btn_layout->addStretch();
    btn_layout->addWidget(btn_Next);
    btn_Next->setFixedSize(145,40);

    auto layout_name_box = new QHBoxLayout;
    layout_name_box->addWidget(l_Name);
    layout_name_box->addStretch();
    layout_name_box->addWidget(line_Name);
    line_Name->setFixedWidth(220);
    box_name->setLayout(layout_name_box);

    auto layout_categories_box = new QHBoxLayout;
    layout_categories_box->addWidget(l_Categories);
    layout_categories_box->addStretch();
    layout_categories_box->addWidget(cbb_Categories);
    cbb_Categories->setFixedWidth(220);
    box_categoties->setLayout(layout_categories_box);

    auto layout_wmclass_box = new QHBoxLayout;
    layout_wmclass_box->addWidget(l_WMClass);
    layout_wmclass_box->addStretch();
    layout_wmclass_box->addWidget(line_WMClass);
    line_WMClass->setFixedWidth(180);
    layout_wmclass_box->addWidget(btn_getWMClass);
    btn_getWMClass->setFixedSize(32,32);
    btn_getWMClass->setIcon(QIcon("://image/xporp.svg"));
    btn_getWMClass->setIconSize(QSize(24,24));
    box_wmclass->setLayout(layout_wmclass_box);

    auto layout_update_box = new QHBoxLayout;
    layout_update_box->addWidget(ckb_update);
    layout_update_box->addStretch();
    layout_update_box->addWidget(cbb_installed);
    cbb_installed->setFixedWidth(220);
    box_update->setLayout(layout_update_box);

    layout->addWidget(box_name);
    layout->addWidget(box_categoties);
    layout->addWidget(box_wmclass);
    layout->addWidget(box_update);
    layout->addLayout(layout_update_box);

    layout->addStretch();
    layout->addLayout(btn_layout);

    btn_Next->setEnabled(false);
    connect(line_Name,&QLineEdit::textChanged,[=](QString text){
        if(text.isEmpty()){
            btn_Next->setEnabled(false);
        }else{
            btn_Next->setEnabled(true);
        }
    });
    connect(btn_getWMClass,&QPushButton::clicked,[=](){
        QProcess p;
        QStringList arg;
        arg.append("WM_CLASS");
        p.start("xprop",arg);
        p.waitForFinished();
        QString id = p.readAll();
        id = id.left(id.lastIndexOf('"'));
        id = id.right(id.length()-id.lastIndexOf('"')-1);
        line_WMClass->setText(id);
    });
    connect(btn_Next,&QPushButton::clicked,[=](){
        BaseInfo tmp;
        tmp.name=line_Name->text(); 
        tmp.categories=cbb_Categories->itemData(cbb_Categories->currentIndex()).toString();
        if(line_WMClass->text() != ""){
            tmp.wmclass=line_WMClass->text();
        }
        emit next(tmp);
    });
}
