#ifndef OPENFILEPAGE_H
#define OPENFILEPAGE_H

#include <QObject>
#include <QWidget>
#include <QLabel>
#include <DFileDialog>
#include <QPushButton>
#include <DSpinner>
#include <DSuggestButton>
#include "src/core/core.h"
#include "page.h"
DWIDGET_USE_NAMESPACE
class OpenFilePage : public Page
{
    Q_OBJECT
public:
    explicit OpenFilePage(QWidget *parent = nullptr);
    void setHover(QLabel *l); //设置点燃的标签
    void open(QString path); //打开文件
    void dragEnterEvent(QDragEnterEvent *event);
    void dropEvent(QDropEvent *event);
    void dragLeaveEvent(QDragLeaveEvent *event);

private:
    QWidget *w_icon;
    QLabel *vndIcon;
    QLabel *iso9660Icon;
    QLabel *filePath;
    DSpinner *spinner;
    QString path;
    QPushButton *btn_OpenFile;
    DSuggestButton *btn_Next;

signals:
    void next(QString path);
public slots:
};

#endif // OPENFILEPAGE_H
