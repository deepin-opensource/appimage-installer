#pragma once
#include <DSimpleListView>
#include <DSimpleListItem>
DWIDGET_USE_NAMESPACE
struct ItemInfo{
    QPixmap fileIcon; //图标
    QString name; //名称
    QString exname; //扩展名
};
class RegFileView : public DSimpleListView{
    Q_OBJECT
public:
    RegFileView(QWidget *parent = nullptr);
    void addItem(QPixmap fileIcon,QString name, QString exname);
};


class RegFileItem : public DSimpleListItem{
    Q_OBJECT
public:
    RegFileItem(QWidget *parent = nullptr);
    RegFileItem(QPixmap icon,QString name,QString exname);

    bool sameAs(DSimpleListItem *item);

    // 绘制Item背景的接口函数，参数依次为表格矩形、绘制QPainter对象、行索引、当前行是否选中？
    void drawBackground(QRect rect, QPainter *painter, int index, bool isSelect, bool isHover);

    // 绘制Item前景的接口函数，参数依次为表格矩形、绘制QPainter对象、行索引、当前行是否选中？
    void drawForeground(QRect rect, QPainter *painter, int column, int index, bool isSelect, bool isHover);
private:
    ItemInfo info;
};