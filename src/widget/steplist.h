#pragma once
#include "src/core/core.h"
#include <QObject>
#include <QWidget>
#include <QPushButton>
#include <DGuiApplicationHelper>
#include <DLabel>

DWIDGET_USE_NAMESPACE
//该类是主界面左侧的菜单栏。
class StepList : public QWidget
{
    Q_OBJECT
public:
    enum Btn{
        OpenFile=1,
        OpenIcon=2,
        SetInfo=3,
        RegFile=4,
        Finish=5,
    };
    explicit StepList(QWidget *parent = nullptr);
    void setNowStep(int i);
private:
    int nowStep;
    QPushButton *btn1_OpenFile;
    QPushButton *btn2_OpenIcon;
    QPushButton *btn3_SetInfo;
    QPushButton *btn4_regFile;
    QPushButton *btn5_finish;
    void updateStyle();
signals:

public slots:
};
