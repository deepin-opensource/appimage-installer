#include "regfileview.h"
#include <DGuiApplicationHelper>
#include <QIcon>
#include <QPainterPath>
RegFileView::RegFileView(QWidget *parent) : DSimpleListView(parent){
    addItem(QIcon::fromTheme("gimp").pixmap(50),"Gimp file","*.xcf");
    // 设置为true时绘制边框
    setFrame(true);

    // 设置边框的圆角是 8像素
    setClipRadius(8);
    // setFrame(true, QColor("#FF0000"), 0.5);
    setMinimumHeight(20);
    setRowHeight(64);
}
RegFileItem::RegFileItem(QWidget *parent) {


}
void RegFileView::addItem(QPixmap fileIcon,QString name, QString exname){
    RegFileItem* newItem=new RegFileItem(fileIcon,name,exname);
    addItems({newItem});
}
RegFileItem::RegFileItem(QPixmap icon,QString name,QString exname){
    info.fileIcon=icon;
    info.name=name;
    info.exname=exname;
}
bool RegFileItem::sameAs(DSimpleListItem *item){
    return static_cast<RegFileItem*>(item)->info.name==info.name &&
        static_cast<RegFileItem*>(item)->info.fileIcon==info.fileIcon &&
        static_cast<RegFileItem*>(item)->info.exname==info.exname;
}
void RegFileItem::drawBackground(QRect rect, QPainter *painter, int index, bool isSelect, bool isHover){
    // 初始化绘制背景所需的行矩形对象
    QPainterPath path;
    path.addRect(QRectF(rect));

    painter->setOpacity(1);
    if (isSelect) {
        painter->fillPath(path, Dtk::Gui::DGuiApplicationHelper::instance()->applicationPalette().highlight().color());
    } else if (index % 2 == 0) {
        if(Dtk::Gui::DGuiApplicationHelper::instance()->themeType()==Dtk::Gui::DGuiApplicationHelper::DarkType){
            painter->fillPath(path, QColor("#323232"));
        }else{
            painter->fillPath(path, QColor("#F8F8F8"));
        }
    }
}
void RegFileItem::drawForeground(QRect rect, QPainter *painter, int column, int index, bool isSelect, bool isHover){
    // 当行选中时使用白色文字，没有选中时使用黑色文字
    painter->setOpacity(1);
    if (isSelect) {
        painter->setPen(QPen(QColor("#FFFFFF")));
    } else {
        if(Dtk::Gui::DGuiApplicationHelper::instance()->themeType()==Dtk::Gui::DGuiApplicationHelper::DarkType){
            painter->setPen(QPen(QColor("#C0C6D4")));
        }else{
            painter->setPen(QPen(QColor("#000000")));
        }
    }
    painter->drawPixmap(rect.x()+rect.height()*0.1,rect.y()+rect.height()*0.1,rect.height()*0.8,rect.height()*0.8,info.fileIcon);
    // 绘制文字，左对齐，纵向居中对齐，文字左边留10像素的空白
    int padding = rect.height();
    painter->drawText(QRect(rect.x() + padding, rect.y(), rect.width() - padding * 2, rect.height()), Qt::AlignLeft | Qt::AlignVCenter, info.name+"("+info.exname+")");
}