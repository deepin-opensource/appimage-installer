#include "steplist.h"
#include <QLayout>
#include <QGraphicsDropShadowEffect>
StepList::StepList(QWidget *parent) : QWidget(parent)
{
    nowStep=1;
    QVBoxLayout* layout=new QVBoxLayout;

    btn1_OpenFile=new QPushButton(tr("打开软件包"),this);
    btn2_OpenIcon=new QPushButton(tr("选择图标文件"),this);
    btn3_SetInfo=new QPushButton(tr("设置基本信息"),this);
    btn4_regFile=new QPushButton(tr("注册文件类型"),this);
    btn5_finish=new QPushButton(tr("完成"),this);

    layout->addSpacing(20);
    layout->addWidget(btn1_OpenFile);
    layout->addWidget(btn2_OpenIcon);
    layout->addWidget(btn3_SetInfo);
    layout->addWidget(btn4_regFile);
    layout->addWidget(btn5_finish);
    layout->addStretch();
    setLayout(layout);

    updateStyle();
    connect(DGuiApplicationHelper::instance(),&DGuiApplicationHelper::themeTypeChanged,this,&StepList::updateStyle);
//    connect(btn1_OpenFile,&QPushButton::clicked,this,[=](){setNowStep(1);});
//    connect(btn2_OpenIcon,&QPushButton::clicked,this,[=](){setNowStep(2);});
//    connect(btn3_SetInfo,&QPushButton::clicked,this,[=](){setNowStep(3);});
//    connect(btn4_regFile,&QPushButton::clicked,this,[=](){setNowStep(4);});
}

void StepList::updateStyle()
{
    setStyleSheet(".QPushButton{"
                  "background:rgba(0,0,0,0);"
//                  "color:#414D68;"
                  "border:none;"
                  "border-radius:10px;"
                  "text-align: left;"
                  "padding-left:30px"
                  "}"
//                  ".QPushButton:hover{"
//                  "background:rgba(240,240,240,0.2);"
//                  "}"
                  "");
    btn1_OpenFile->setFixedHeight(42);
    btn2_OpenIcon->setFixedHeight(42);
    btn3_SetInfo->setFixedHeight(42);
    btn4_regFile->setFixedHeight(42);
    btn5_finish->setFixedHeight(42);
    setNowStep(nowStep);
}

void StepList::setNowStep(int i)
{
    nowStep=i;
    btn1_OpenFile->setStyleSheet("");
    btn2_OpenIcon->setStyleSheet("");
    btn3_SetInfo->setStyleSheet("");
    btn4_regFile->setStyleSheet("");
    btn5_finish->setStyleSheet("");
    btn1_OpenFile->setGraphicsEffect(nullptr);
    btn2_OpenIcon->setGraphicsEffect(nullptr);
    btn3_SetInfo->setGraphicsEffect(nullptr);
    btn4_regFile->setGraphicsEffect(nullptr);
    btn5_finish->setGraphicsEffect(nullptr);

    QGraphicsDropShadowEffect *shadow_effect = new QGraphicsDropShadowEffect(this);
    shadow_effect->setOffset(2, 2);
    QColor c=Core::highlightColor();
    c.setAlpha(100);
    shadow_effect->setColor(c);
    shadow_effect->setBlurRadius(10);
    QString style="background-color:"+Core::highlightColor().name()+";"
                  "color:#ffffff;";
    switch (i) {
    case Btn::OpenFile:
        btn1_OpenFile->setStyleSheet(style);
        btn1_OpenFile->setGraphicsEffect(shadow_effect);
        break;
    case Btn::OpenIcon:
        btn2_OpenIcon->setStyleSheet(style);
        btn2_OpenIcon->setGraphicsEffect(shadow_effect);
        break;
    case Btn::SetInfo:
        btn3_SetInfo->setStyleSheet(style);
        btn3_SetInfo->setGraphicsEffect(shadow_effect);
        break;
    case Btn::RegFile:
        btn4_regFile->setStyleSheet(style);
        btn4_regFile->setGraphicsEffect(shadow_effect);
        break;
    case Btn::Finish:
        btn5_finish->setStyleSheet(style);
        btn5_finish->setGraphicsEffect(shadow_effect);
        break;
    }
}
