
#include "appimage.h"

AppImage *AppImage::file = nullptr;
AppImage::AppImage(QObject *parent) : QObject(parent)
{
    return;
}
/*!
 * \brief AppImage::load
 * \param path
 * \return self or null(fail)
 */
AppImage *AppImage::load(QString path)
{
    QProcess *unpack = new QProcess;
//    unpack->deleteLater();

    qDebug() << "tmpPath:" << Core::tmpDir().path();
    qDebug() << "appRootPath:" << Core::appRootDir().path();
    //两种类型重合的逻辑
    auto loadMain = [=]() {
        this->m_path = path;
        //获取图标
        QProcess ls;
        ls.start("ls", {Core::appRootDir().path()});
        ls.waitForFinished();
        QStringList fileicon = QString::fromUtf8(ls.readAllStandardOutput()).split("\n");
        for (int i = 0; i < fileicon.size(); i++)
        {
            QString iconPath = Core::appRootDir().path() + "/" + fileicon[i];
            if (Core::getFileType(iconPath) == Core::TypePng)
            {
                m_icon.path = iconPath;
                m_icon.pixmap = QPixmap(iconPath);
            }
            else if (Core::getFileType(iconPath) == Core::TypeSvg)
            {
                m_icon.path = iconPath;
                m_icon.pixmap = Core::loadSvg(iconPath);
            }
        }
        //扫描mime
        QStringList mimes = Core::lsDir(Core::appRootDir().path() + "/usr/share", Core::TypeXml, Core::LsDirOption::allDir);
        for (auto m : mimes)
        {
            // qDebug() << "MimeReg >" << m;
            MimeInfo info = Core::loadMineInfo(m);
            if (info.comment != "" &&
                info.exname != "" &&
                info.type[0] != "" &&
                info.path != "")
            {
                if (info.iconName == "")
                {
                    info.iconName = "unknown";
                }
                regFiles << info;
            }
        }
        emit AppImage::loadFinish();
    };
    connect(unpack, QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished),
            [=](int exitCode, QProcess::ExitStatus exitStatus) {
                Q_UNUSED(exitCode)
                Q_UNUSED(exitStatus)
                QtConcurrent::run(loadMain);
            });
    if (Core::getFileType(path) == Core::FileType::TypeAppVnd)
    {
        QProcess chmod;
        QStringList args;
        args << "+x" << path;
        chmod.start("chmod", args);
        chmod.waitForFinished();
        args.clear();
        args << "--appimage-extract";
        unpack->start(path, args);
    }
    else if (Core::getFileType(path) == Core::FileType::TypeAppIso9660)
    {
        Core::tmpDir().mkdir("squashfs-root");
        QStringList args;
        args << "-p" << path << Core::appRootDir().path();
        unpack->start("fuseiso", args);
        qDebug() << "out:" << unpack->readAll();
    }
    else
    {
        return nullptr;
    }

    return this;
}
bool AppImage::install()
{
    QDir dataDir(Core::env("XDG_DATA_HOME")[0]);
    if (!(dataDir.mkpath("appimages/icons") && dataDir.mkpath("appimages/homes") && dataDir.mkpath("applications")))
    {
        qDebug()<<"INSTALL-WARRING:创建安装所需文件夹";
    }

    QDir target_app(dataDir.path() + "/appimages");         //用于存放App可执行文件
    QDir target_icon(dataDir.path() + "/appimages/icons");  //用于存放手动添加的图标目录
//    QDir target_homes(dataDir.path() + "/appimages/homes"); //用于存放各个虚拟目录
    QDir target_desktop(dataDir.path() + "/applications");  //用于输出desktop文件的目录
    QDir target_mime(dataDir.path() + "/mime"); //xml文件类型描述文件路径
    QFile app_file(path());
    QFileInfo app_fileInfo(app_file);
    qDebug()<<dataDir.path();
    if (!app_file.open(QIODevice::OpenModeFlag::ReadOnly))
    {
        qDebug()<<"INSTALL-ERROR:打开软件包";
        return false;
    }
    if (!app_file.copy(target_app.path() + "/" + app_fileInfo.fileName()))
    {
        qDebug()<<"INSTALL-ERROR:复制软件包"<<target_app.path() + "/" + app_fileInfo.fileName();
        return false;
    }
    QProcess chmod;
    QStringList args;
    args<<"+x"<<target_app.path()+"/"+app_fileInfo.fileName();
    chmod.start("chmod",args);
    chmod.waitForFinished();
    if(!chmod.readAllStandardError().isEmpty()){
        qDebug()<<"INSTALL-ERROR:为软件包添加执行权限";
        return false;
    }
    app_file.close();
    QString desktop_text;
    QString icon_text;
    if (icon().type == FromFile)
    {
        QFile ico(icon().path);
        QFileInfo ico_info(ico);
        if(!ico.copy(target_icon.path() + "/" + ico_info.fileName())){
            qDebug()<<"INSTALL-WARRING:复制图标文件";
        }
        icon_text = target_icon.path() + "/" + ico_info.fileName();
    }else
        icon_text = icon().path;
    desktop_text=Core::mkDesktop(name(),
                                icon_text,
                                target_app.path()+"/"+app_fileInfo.fileName(),
                                wmClass(),
                                categories());
    target_app.mkpath("homes/"+app_fileInfo.fileName()+"_home");

    qDebug()<<"----Desktop----\n"<<desktop_text<<"-------------\n";
    QFile desktop_file(target_desktop.path()+"/"+name()+".desktop");
    if(desktop_file.open(QIODevice::ReadWrite)){
        desktop_file.write(desktop_text.toUtf8());
    }else{
        qDebug()<<"INSTALL-ERROR:写入快捷方式";
        return false;
    }
    for(auto t : regFiles){
        QFile tf(t.path);
        QFileInfo ti(t.path);
        tf.open(QFile::ReadWrite);
        tf.copy(target_mime.path()+"/"+t.type[0]+"/"+ti.fileName());
        qDebug()<<"INSTALL-OUT:"<<target_mime.path()+"/"+t.type[0]+"/"+ti.fileName();
    }
    // system("update-mime-database "+dataDir.path().toUtf8());

    return true;
}

QString AppImage::wmClass()
{
    return m_wmclass;
}

void AppImage::setWMClass(QString wmclass)
{
    m_wmclass = wmclass;
}
Core::FileType AppImage::Type()
{
    return Core::getFileType(m_path);
}

AppIcon AppImage::icon()
{
    return m_icon;
}
void AppImage::setIcon(AppIcon icon)
{
    m_icon = icon;
}
QString AppImage::path()
{
    return m_path;
}
void AppImage::setName(QString name){
    m_name=name;
}
QString AppImage::name(){
    return m_name;
}
QString AppImage::categories(){
    return m_categories;
}
void AppImage::setCategories(QString c){
    m_categories=c;
}
