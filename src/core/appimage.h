#pragma once
#include <src/core/core.h>
#include <QObject>
#include <QPixmap>
#include <QSvgRenderer>
#include <QPainter>
#include <QProcess>
#include <QtConcurrent>
enum IconType{
    FromFile=1,
    FromTheme=0
};
struct AppIcon{
    QPixmap pixmap;
    QString path;
    IconType type;
};
class AppImage : public QObject
{
    Q_OBJECT
public:
    static AppImage *file;
    explicit AppImage(QObject *parent = nullptr);
    AppImage* load(QString path);
    Core::FileType Type();
    AppIcon icon();
    void setIcon(AppIcon icon);
    QString name();
    void setName(QString name);
    QString categories();
    void setCategories(QString c);
    QString path();
    QList<MimeInfo> regFiles;
    void setName();
    bool install();
    QString wmClass();
    void setWMClass(QString wmclass);
private:
    QString m_path;
    AppIcon m_icon;
    QString m_name;
    QString m_categories;
    QString m_wmclass;
signals:
    void loadFinish();
public slots:
};
