#include "core.h"
#include <QRgb>
#include <QFile>
#include <QSettings>
#include <QApplication>
#include <DSysInfo>
QTemporaryDir* Core::m_tempDir = new QTemporaryDir();
DWIDGET_USE_NAMESPACE
Core::Core(QObject *parent) : QObject(parent)
{
}

bool Core::init()
{
    if(!m_tempDir->isValid()){
        qDebug() << "ERROR:临时文件及创建失败";
        return false;
    }

    QDir dataDir(Core::env("XDG_DATA_HOME")[0]);
    QDir target_app(dataDir.path() + "/appimages");         //用于存放App可执行文件
    QDir::setCurrent(tmpDir().path());
    autoRemoveApp();
    return true;
}
bool Core::isUOSDevMode(){
    QFile devMode("/var/lib/deepin/developer-mode/enabled");
    devMode.open(QFile::ReadOnly);
    QString dev(devMode.readAll());
    if(!Dtk::Core::DSysInfo::isCommunityEdition()){
        if(dev.left(1)!="1"){
            return false;
        }
    }
    return true;
}

void Core::close()
{
    m_tempDir->remove();
}
/*!
 * \brief Core::getFileType
 * \param path
 * \return  FileType(文件类型标志)
 */
Core::FileType Core::getFileType(QString path)
{
    QMimeDatabase db;
    QString type = db.mimeTypeForFile(path, QMimeDatabase::MatchContent).name();
    // qDebug() << "MimeType>" << path << type;
    if (type == "application/vnd.appimage")
    {
        return FileType::TypeAppVnd;
    }
    else if (type == "application/x-iso9660-appimage")
    {
        return FileType::TypeAppIso9660;
    }
    else if (type == "image/svg+xml")
    {
        return FileType::TypeSvg;
    }
    else if (type == "image/png")
    {
        return FileType::TypePng;
    }
    else if (type == "application/xml")
    {
        return FileType::TypeXml;
    }
    else if (type == "inode/directory")
    {
        return FileType::TypeDir;
    }
    else
    {
        return FileType::TypeOther;
    }
}

QDir Core::tmpDir()
{
    return QDir(m_tempDir->path());
}

QDir Core::appRootDir()
{
    return QDir(m_tempDir->path() + "/squashfs-root");
}
QString Core::mkDesktop(QString name, QString icon, QString Exec, QString wmclass, QString cate)
{
    QString in;
    in += "[Desktop Entry]\n";
    in += "Encoding=UTF-8\n";
    in += "Type=Application\n";
    in += "Name=" + name + "\n";
    in += "Icon=" + icon + "\n";
    in += "Categories=" + cate + "\n";
    in += "Exec=\"" + Exec + "\"\n";
    in += "StartupWMClass=" + wmclass +"\n";
    return in;
}
QString Core::mkDesktop(QString name, QString icon, QString Exec, QString wmclass, Categories cate)
{
    QString categories;
    switch (cate)
    {
    case Core::Categories::Other:
        categories = "Other";
        break;
    case Core::Categories::Chat:
        categories = "Chat";
        break;
    case Core::Categories::Audio:
        categories = "Audio";
        break;
    case Core::Categories::Video:
        categories = "AudioVideo";
        break;
    case Core::Categories::Graphics:
        categories = "Graphics";
        break;
    case Core::Categories::Game:
        categories = "Game";
        break;
    case Core::Categories::OfficeAndStudy:
        categories = "Office";
        break;
    case Core::Categories::Translation:
        categories = "Translation";
        break;
    case Core::Categories::Development:
        categories = "Development";
        break;
    case Core::Categories::System:
        categories = "System";
        break;
    case Core::Categories::Internet:
        categories = "Network";
        break;
    }
    QString in = mkDesktop(name, icon, Exec, categories, wmclass);
    return in;
}

void Core::clearTmp(bool onlyMine)
{
    if (onlyMine)
    {
        QStringList args;
        args << "-u";
        args << appRootDir().path();
        QProcess umount;
        umount.start("fusermount", args);
        umount.waitForFinished();
        system("rm -rf " + appRootDir().path().toUtf8());
    }
    else
    {
        int a = 1;
        QString t;
        do
        {
            t = QDir::temp().path() + "/AppImageInstaller_" + QString::number(a) + "/squashfs-root";
            a++;
            QStringList args;
            args << "-u";
            args << t;
            QProcess umount;
            umount.start("fusermount", args);
            umount.waitForFinished();
        } while (Core::isExist(t));
        system("rm -rf /tmp/" + QString(TMP_DIR_PERFIX).toUtf8() + "*");
    }
}

QFont Core::font(Core::FontType type)
{
    QFont font;
    switch (type)
    {
    case FontType::TitleFont:
        font.setPointSize(24);
        break;
    case FontType::TipFont:
        font.setPointSize(12);
        break;
    }
    return font;
}

QColor Core::highlightColor(int alpha)
{
    QColor c = DGuiApplicationHelper::instance()->applicationPalette().highlight().color();
    c.setAlpha(alpha);
    return c;
}

QColor Core::pixmapMainColor(QPixmap p, float bright)
{
    int w = p.width();
    int h = p.height();
    int r = 0, g = 0, b = 0, a = 0;
    int t = 0;
    QImage image = p.toImage();
    for (int i = 0; i < w; i += 5)
    {
        for (int j = 0; j < h; j += 5)
        {
            if (image.valid(i, j))
            {
                t++;
                QColor c = image.pixel(i, j);
                r += c.red();
                g += c.green();
                b += c.blue();
                a += c.alpha();
            }
        }
    }
    return QColor(int(bright * r / t), int(bright * g / t), int(bright * b / t), a / t);
}

QString Core::openFile(QString title, QString filter)
{
    QFileDialog dlg;
    return dlg.getOpenFileName(nullptr, title, QDir::homePath(), filter);
    ;
}

bool Core::isExist(QString path)
{
    std::fstream tmp;
    tmp.open(path.toUtf8(), std::ios::in);
    bool a;
    if (tmp)
    {
        a = true;
    }
    else
    {
        a = false;
    }
    tmp.close();
    return a;
}

QPixmap Core::loadSvg(QString path, int w, int h)
{
    QSvgRenderer *svgRender = new QSvgRenderer();
    svgRender->load(path);
    QPixmap pixmap;
    pixmap = QPixmap(w, h);
    pixmap.fill(Qt::transparent); //设置背景透明
    QPainter p(&pixmap);
    svgRender->render(&p);
    delete svgRender;
    return pixmap;
}
MimeInfo Core::loadMineInfo(QString path)
{
    QFile file(path);
    MimeInfo info;
    if (file.open(QFile::ReadOnly | QFile::Text))
    {
        QXmlStreamReader reader(&file);
        while (!reader.isStartElement())
        {
            reader.readNext();
        }
        if (reader.name() != "mime-type" && reader.name() != "mime-info")
        {
            qDebug() << reader.name() << ":Not";
            return info;
        }
        while (!reader.atEnd())
        {

            QXmlStreamAttributes att = reader.attributes();
            if (reader.isStartElement())
            {
                if (att.hasAttribute("type") && reader.name() == "mime-type")
                {
                    info.type = att.value("type").toString().split('/');
                }
                if (att.hasAttribute("pattern") && reader.name() == "glob")
                {
                    info.exname = att.value("pattern").toString();
                }
                if (att.hasAttribute("name") && reader.name() == "icon")
                {
                    info.iconName = att.value("name").toString();
                }
                else if (att.hasAttribute("name") && reader.name() == "generic-icon")
                {
                    info.iconName = att.value("name").toString();
                }
                if (reader.name() == "comment")
                {
                    info.comment = reader.readElementText();
                }
            }
            reader.readNext();
        }
        info.path = path;
    }
    return info;
}
QStringList Core::lsDir(QString dir, FileType mime, LsDirOption option)
{
    QProcess ls;
    ls.start("ls", {dir});
    ls.waitForFinished();
    QStringList filelist = QString::fromUtf8(ls.readAllStandardOutput()).split("\n");
    QStringList out;
    for (auto filename : filelist)
    {
        if (filename == "")
        {
            continue;
        }
        //判断最后的符号
        qDebug()<<"dir"<<dir<<"filename"<<filename;
        QString path;
        path = dir + "/" + filename;
        path.replace("//","/");
        FileType type = Core::getFileType(path);
        if (mime == FileType::TypeAll)
        {
            out << path;
        }
        else if (type == mime)
        {
            out << path;
        }
        if (option == LsDirOption::allDir && type == FileType::TypeDir)
        {
            for (auto o : lsDir(path, mime, LsDirOption::allDir))
            {
                out << o;
            }
        }
    }
    return out;
}
QStringList Core::env(QString key){
    QString tmp=QProcessEnvironment::systemEnvironment().value(key);
    return tmp.split(':');
}
void Core::autoRemoveApp(){

    QDir dataDir(Core::env("XDG_DATA_HOME")[0]);
    QDir target_app(dataDir.path() + "/appimages");         //用于存放App可执行文件
    QDir target_desktop(dataDir.path() + "/applications");  //desktop文件的目录


    if(!target_app.exists())
        return;

    //取到所有的文件和文件名，去掉.和..文件夹
    target_app.setFilter( QDir::Files | QDir::NoDotAndDotDot);
    //将其转化为一个list
    QFileInfoList list = target_app.entryInfoList();
    if(list.size()==0)
        return;

    int i = 0;
    //采用递归算法
    do {
        QFileInfo fileInfo = list[i];
        QString appimage_file=fileInfo.filePath();
        if(getFileType(appimage_file)==FileType::TypeAppIso9660 || getFileType(appimage_file)==FileType::TypeAppVnd){

            if(!target_desktop.exists())
                return;

            //取到所有的文件和文件名，去掉.和..文件夹
            target_desktop.setFilter(QDir::Files | QDir::NoDotAndDotDot);
            //将其转化为一个list
            QFileInfoList list_desktop = target_desktop.entryInfoList();
            if(list_desktop.size()==1)
                return;
            int j = 0;
            //采用递归算法
            bool isNormal=false;
            QStringList tmp_desktop_list;
            do {
                QFileInfo fileInfo2 = list_desktop[j];
                QString desktop_file=fileInfo2.filePath();
                if(desktop_file.right(8)==".desktop"){
                    QSettings desktop_ini(desktop_file,QSettings::IniFormat);
                    tmp_desktop_list=desktop_ini.value("Desktop Entry/Exec").toUrl().toString().split(" ");
                    for(int i=0;i<tmp_desktop_list.size();i++){
                        if(appimage_file==tmp_desktop_list[i]){
                            isNormal=true;
                            break;
                        }
                    }
                    
                }

                ++j;
            }while(j<list_desktop.size());
            if(!isNormal){
                qDebug()<<"Remove:"<<appimage_file;
                QFile::remove(appimage_file);
                // //app_fileInfo.fileName()+"_home"
                // QDir dir(target_app.path()+"/homes");
                // dir.remove(appimage_file+"_home");
            }
        }
        ++i;
    }while(i<list.size());
}
