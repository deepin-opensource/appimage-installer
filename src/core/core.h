#pragma once
#include <fstream>

#include <QObject>
#include <QMimeType>
#include <QMimeData>
#include <QMimeDatabase>
#include <QDragEnterEvent>
#include <QDragLeaveEvent>
#include <QGraphicsDropShadowEffect>
#include <QSvgRenderer>
#include <QFileDialog>
#include <QDir>
#include <QDebug>
#include <QProcess>
#include <QFont>
#include <DGuiApplicationHelper>
#include <DLabel>
#include <QColor>
#include <QXmlStreamReader>
#include <QTemporaryDir>
#include <QProcessEnvironment>
#define TMP_DIR_PERFIX "AppImageInstaller_"

DWIDGET_USE_NAMESPACE
struct MimeInfo {
    QString comment;
    QString exname;
    QStringList type;
    QString iconName;
    QString path;
};
class Core : public QObject
{
    Q_OBJECT
public:
    explicit Core(QObject *parent = nullptr);

    enum FileType{
        TypeDir=-1,
        TypeAppVnd=1,
        TypeAppIso9660=2,
        TypeSvg=3,
        TypePng=4,
        TypeXml=5,
        TypeOther=0,
        TypeAll=0,};
    enum Categories{
        Other=0,
        Internet=10,
        Chat=1,
        Audio=2,
        Video=3,
        Graphics=4,
        Game=5,
        OfficeAndStudy=6,
        Translation=7,
        Development=8,
        System=9,
    };
    enum FontType{
        TitleFont=1,
        TipFont=2
    };
    enum LsDirOption{
        thisDir=1, //只列出当前目录下的文件和文件夹
        allDir=2,  //递归列出所有目录的文件和文件夹
    };
    static bool init(); //初始化Core模块
    static FileType getFileType(QString path); //通过文件路径获取文件类型
    static QDir tmpDir(); //获取临时文件夹
    static QDir appRootDir(); //获取软件解包文件夹
    static QString mkDesktop(QString name,QString icon,QString Exec,QString wmclass, Categories cate=Categories::Other); //生成Desktop文本
    static QString mkDesktop(QString name,QString icon,QString Exec,QString wmclass,QString cate=""); //生成Desktop文本
    static void clearTmp(bool onlyMine=false); //清除临时文件
    static QFont font(FontType); //各个UI部分的字体，主要是大小
    static QColor highlightColor  (int alpha=255); //获取活动用色
    static QColor pixmapMainColor(QPixmap p,float bright=1); //获取图片的平均颜色，参数bright为亮度系数
    static QString openFile(QString title,QString filter); //使用打开文件对话框选择文件
    static bool isExist(QString path); //判断文件或着文件夹是否存在
    static QPixmap loadSvg(QString path,int w=128,int h=128); //加载Svg图片到QPixmap
    static MimeInfo loadMineInfo(QString path); //加载xml文件，解析内容
    static QStringList lsDir(QString dir, FileType mime=FileType::TypeAll , LsDirOption option=LsDirOption::thisDir); //获取当前文件夹下文件列表
    static QStringList env(QString key); //获取环境变量的值
    static void autoRemoveApp();//自动扫描并删除没有被desktop文件指向的软件

    static bool isUOSDevMode();//判断当前是否已经开启开发者模式
    static void close(); //释放资源，
private:
//    static QDir m_tmpDir;
    static QTemporaryDir *m_tempDir;
signals:

public slots:
};
