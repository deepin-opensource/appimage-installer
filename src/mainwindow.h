#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QLabel>
#include <QStackedWidget>

#include <DBlurEffectWidget>
#include <DTitlebar>
#include "src/widget/steplist.h"
#include "src/core/appimage.h"
#include "src/page/openfilepage.h"
#include "src/page/openiconpage.h"
#include "src/page/setbaseinfopage.h"
#include "src/page/regfiletypepage.h"
#include "src/page/installpage.h"
DWIDGET_USE_NAMESPACE

class MainWindow : public DBlurEffectWidget
{
    Q_OBJECT
public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void openFile(QString path);
private:
    DTitlebar* titlebar;
    QStackedWidget* stk;
    StepList* stepList;
    void updateStyle();
    OpenFilePage* openFilePage;
    OpenIconPage* openIconPage;
    SetBaseInfoPage* setBaseInfoPage;
    RegFileTypePage* regFileTypePage;
    InstallPage* installPage;
};

#endif // MAINWINDOW_H
