#include "mainwindow.h"
#include <DApplication>
#include "src/core/core.h"
#include "src/core/appimage.h"
#include <DDialog>
#include <QTranslator>
#include <DApplicationSettings>
DWIDGET_USE_NAMESPACE
int main(int argc, char *argv[])
{
    DApplication a(argc, argv);
    a.setOrganizationName("Maicss");
    a.setApplicationName("appimage-installer");
    a.setProductIcon(QIcon::fromTheme("appimage-installer"));
    QFile version(a.applicationDirPath()+"/version");
    version.open(QFile::ReadOnly);
    a.setApplicationVersion(version.readAll());
    DApplicationSettings as;
    QTranslator translator;
    if(!translator.load(a.applicationDirPath()+"/translations/appimage-installer_" + QLocale::system().name()) && QLocale::system().name()!="zh_CN"){
        translator.load(a.applicationDirPath()+"/translations/appimage-installer_en_US.qm");
    }
    a.loadTranslator();
    a.installTranslator(&translator);
    if(!Core::isUOSDevMode()){
        DDialog dlg;
        dlg.setTitle("开发者模式未打开");
        dlg.setMessage("检测到您当前并未打开开发者模式，由于UOS的安全机"
                        "制影响，系统不允许运行未经签名的软件，因此，您将"
                        "不能正常使用大部分AppImage格式的软件，这也将导"
                        "致我们的安装失效。如果您需要打开开发者模式，可以"
                        "通过控制中心->通用->开发者模式中打开。");
        dlg.addButton("知道了",true,Dtk::Widget::DDialog::ButtonType::ButtonRecommend);
        dlg.exec();
        return 0;
    }
//     Core::clearTmp();
    if(!Core::init()) return -1;
    AppImage::file=new AppImage;
    MainWindow w;
    w.show();
    if(argc==2)
        w.openFile(argv[1]);
    return a.exec();
}
