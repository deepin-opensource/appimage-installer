#-------------------------------------------------
#
# Project created by QtCreator 2021-07-16T15:02:18
#
#-------------------------------------------------

QT       += core gui svg concurrent

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = appimage-installer
TEMPLATE = app
TRANSLATIONS = \
    translator/appimage-installer_en_US.ts
# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11  link_pkgconfig
PKGCONFIG +=dtkwidget

SOURCES += \
        src/main.cpp \
        src/mainwindow.cpp \
    src/core/appimage.cpp \
    src/core/core.cpp \
    src/page/installpage.cpp \
    src/page/openfilepage.cpp \
    src/page/openiconpage.cpp \
    src/page/page.cpp \
    src/page/regfiletypepage.cpp \
    src/page/setbaseinfopage.cpp \
    src/widget/regfileview.cpp \
    src/widget/steplist.cpp \
    src/core/installedapp.cpp

HEADERS += \
        src/mainwindow.h \
    src/core/appimage.h \
    src/core/core.h \
    src/page/installpage.h \
    src/page/openfilepage.h \
    src/page/openiconpage.h \
    src/page/page.h \
    src/page/regfiletypepage.h \
    src/page/setbaseinfopage.h \
    src/widget/regfileview.h \
    src/widget/steplist.h \
    src/core/installedapp.h

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    res.qrc
