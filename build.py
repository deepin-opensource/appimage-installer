#!/bin/python3
#coding=utf-8
# 本脚本用于自动构建为deb软件包
import os,sys
import shutil

# 读取版本号
vfile = open("./version","r+")
version = vfile.read()
print("版本号:",version)
# 开始构建包
print("开始构建...")
buildDir="./build"
if not os.path.exists(buildDir):
    os.mkdir(buildDir)
os.chdir(buildDir)
if os.system("qmake ..")!=0 :
    print("qmake失败")
    exit(-1)
if os.system("make")!=0 :
    print("make失败")
    exit(-1)
print("构建完成")
# 开始构建包目录
print("开始打包...")
debDir = "deb"
if os.path.exists(debDir):
    shutil.rmtree(debDir)
shutil.copytree("../debian",debDir)
# 开始编辑control
controlFile=open(debDir+"/DEBIAN/control","r+")
controlText=controlFile.read().replace("<version>",version)
controlFile.close()
controlFile=open(debDir+"/DEBIAN/control","w+")
print(controlText)
controlFile.write(controlText)
controlFile.close()
# 开始编辑info文件
infoFile=open(debDir+"/opt/apps/com.appimage-installer/info","r+")
infoText=infoFile.read().replace("<version>",version)
infoFile.close()
infoFile=open(debDir+"/opt/apps/com.appimage-installer/info","w+")
print(infoText)
infoFile.write(infoText)
infoFile.close()
# 部署图标文件
os.makedirs(debDir+"/opt/apps/com.appimage-installer/entries/icons/hicolor/scalable/apps/")
shutil.copy("../image/logo/appimage-installer.svg",debDir+"/opt/apps/com.appimage-installer/entries/icons/hicolor/scalable/apps/appimage-installer.svg")
# 部署可执行文件
os.makedirs(debDir+"/opt/apps/com.appimage-installer/files")
shutil.copy("../runapp.py",debDir+"/opt/apps/com.appimage-installer/files/runapp.py")
shutil.copy("appimage-installer",debDir+"/opt/apps/com.appimage-installer/files/appimage-installer")
# 部署翻译文件
os.system("lrelease ../translations/*.ts")
os.makedirs(debDir+"/opt/apps/com.appimage-installer/files/translations")
os.system("cp ../translations/*.qm "+debDir+"/opt/apps/com.appimage-installer/files/translations/")
# 部署数据文件
os.system("cp ../data/* "+debDir+"/opt/apps/com.appimage-installer/files/")
# 构建deb软件包
if os.system("fakeroot dpkg -b deb ../appimage-installer_"+version+"_amd64.deb")!=0:
    print("构建失败！")


os.chdir("..")
#询问是否删除构建目录
yesorno = input("是否删除构建的临时目录(y/N)？")
if yesorno in {"Y","y"}:
    shutil.rmtree(buildDir)
