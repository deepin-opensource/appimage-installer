# appimage-installer

#### 介绍
一个非常方便的appimage软件部署工具

#### 新版特性

1.新增文件Mime注册

#### 软件架构
软件使用DTK开发套件开发

#### 编译和安装

```bash
./build.sh
```

运行结束后会在当前目录下生成文件夹build和一个deb文件，

这个deb包仅仅支持deepin/UOS的安装和正常使用。

#### 功能说明

软件本身提供AppImage的解包功能，目的是可以快速方便的获取包中的图标等信息。

软件部署的具体目录如下：

```
软件主体：$XDG_DATA_HOME/appimages

软件图标：$XDG_DATA_HOME/appimages/icons

快捷方式：$XDG_DATA_HOME/applications
```

只有在你通过文件选择图标时，才会部署到相应目录，通过主题选择时不会在图标目录产生任何文件。

为了方便起见，软件菜单中可以快速打开`$XDG_DATA_HOME/appimages（软件文件夹）`和`$XDG_DATA_HOME/applications（快捷方式文件夹）`两个文件夹，这用于删除或者修改已经部署的软件。

#### 参与贡献

1.  Fork 本仓库
